package progressivePagesTest;

import org.testng.annotations.Test;
import org.testng.Assert;
//import org.testng.Assert;
//import org.testng.AssertJUnit;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.openqa.selenium.NoSuchElementException;
//import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

//import config.PropertiesFile;
import progressivePages.AddVehiclePage;
import progressivePages.DriverInformationPage;
import progressivePages.HomePage;
import progressivePages.InsuranceDetailPage;
import progressivePages.PersonalInformationPage;
import progressivePages.ZipCodePage;
import progressiveUtils.ScreenshotUtility;

public class StartAQuoteTestNg {

	static WebDriver driver;

	// Store value of browser name
	static String browserName = null;

	static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

	@BeforeTest
	public void BeforeTest() {

		// Reads configuration file and invoke driver(browser) based on configuration

		PropertiesFile.getProperties();

		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
			driver = new ChromeDriver();
		}

		else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./libs/geckodriver");
			driver = new FirefoxDriver();

		}

	}

	@Test(priority = 0)
	public void startAQuoteTest() throws InterruptedException {
		try {
			// Create an object of HomePage to utilize UI elements

			HomePage home = new HomePage(driver);
			// Creating new ExtentReport object and creating new ExtentReport.html page
			ExtentHtmlReporter htmlRepoter = new ExtentHtmlReporter("extentReports.html");

			// Create ExtentReports and attach the reporter
			ExtentReports extent = new ExtentReports();
			extent.attachReporter(htmlRepoter);

			// Create a toggle for the given test and adds log event under it.
			ExtentTest test = extent.createTest("Progressive Insurance Test",
					"This will check and see if we can perform various operations");

			test.log(Status.INFO, "Executing Progressive page test");

			driver.get("https://www.progressive.com/");
			driver.manage().window().maximize();

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			System.out.println(timestamp);

			if (driver.getTitle().contains("Quote Auto Insurance, Home-Auto Bundles, & More | Progressive")) {
				test.pass("Navigate to Home Page");
			} else {
				test.fail("Error loading home page");
			}

			// Validate if home page is opened
			Assert.assertTrue(home.pageTitle());

			// Take screenshot of Progressive HomePage
			ScreenshotUtility.takeSnapShot(driver, "Progressive Homepage");

			home.auto();

			// Create an object of ZipCodePage to utilize UI elements
			ZipCodePage zipcode = new ZipCodePage(driver);

			zipcode.zipCode("75067");

			ScreenshotUtility.takeSnapShot(driver, "Progressive ZipCodePage");

			zipcode.submitQuote();
			Thread.sleep(5000);

			test.log(Status.INFO, "Executing PersonalInformationPage test");

			// Create an object of PersonalInformationPage to utilize UI elements
			PersonalInformationPage personalInfo = new PersonalInformationPage(driver);

			if (driver.getTitle().contains("Name And Address")) {
				test.pass("Navigate to Personal Information Page");
			} else {
				test.fail("Error loading Personal Information Page");
			}

			personalInfo.firstName("Test");
			personalInfo.lastName("Testing");
			personalInfo.suffix("Jr");
			personalInfo.dateOfBirth("03091989");
			Thread.sleep(3000);
			personalInfo.streetName("350 East vista ridge");
			Thread.sleep(2000);
			personalInfo.apartmentNumber("240");

			ScreenshotUtility.takeSnapShot(driver, "Progressive InformationPage");
			personalInfo.selectButton();

			Thread.sleep(5000);

			test.log(Status.INFO, "Executing AddVehiclePage test");
			// Create an object of AddVehiclePage to utilize UI elements
			AddVehiclePage addVehicle = new AddVehiclePage(driver);

			if (driver.getTitle().contains("Add Vehicle")) {
				test.pass("Navigate to AddVehiclePage");
			} else {
				test.fail("Error loading AddVehiclePage");
			}

			Actions builder = new Actions(driver);
			builder.moveToElement(addVehicle.selectVehicleYear()).build().perform();
			// addVehicle.selectVehicleYear();
			Thread.sleep(2000);
			builder.moveToElement(addVehicle.selectVehicleMake()).build().perform();

			// addVehicle.selectVehicleMake();
			Thread.sleep(2000);
			builder.moveToElement(addVehicle.selectVehicleModel()).build().perform();
			// addVehicle.selectVehicleModel();
			addVehicle.selectVehicleBodyType("SUV (4CYL 4X4)");
			Thread.sleep(2000);
			addVehicle.selectPrimaryUseOption("Personal (to/from work or school, errands, pleasure)");
			addVehicle.selectCheckbox();
			Thread.sleep(2000);
			addVehicle.selectOwnOrLease("Own");
			Thread.sleep(2000);
			addVehicle.vehiclePeriod("1 month - 1 year");
			addVehicle.selectDriverAssistance();
			addVehicle.selectBlindSpotWarning();

			ScreenshotUtility.takeSnapShot(driver, "Progressive AddVehiclePage");

			Thread.sleep(2000);
			addVehicle.selectDoneButton();
			Thread.sleep(2000);
			addVehicle.selectContinueButton();
			Thread.sleep(2000);

			test.log(Status.INFO, "Executing DriverInformationPage test");

			// Create an object of DriverInformationPage to utilize UI elements
			DriverInformationPage driverInfo = new DriverInformationPage(driver);

			if (driver.getTitle().contains("Policyholder Details")) {
				test.pass("Navigate to DriverInformationPage");
			} else {
				test.fail("Error loading DriverInformationPage");
			}

			driverInfo.chooseGender();
			driverInfo.selectMaritalStatus("Single");
			Thread.sleep(2000);
			driverInfo.selectEducationLevel("College degree");
			Thread.sleep(2000);
			driverInfo.selectEmploymentStatus("Employed");
			Thread.sleep(2000);
			driverInfo.enterOccupation("Engineer: IT Applications / Software");
			Thread.sleep(2000);
			driverInfo.ssNumber("1234567890");
			Thread.sleep(2000);
			driverInfo.selectPrimaryResidency("Rent");
			Thread.sleep(2000);
			driverInfo.selectMovedSomewhere("No");
			Thread.sleep(2000);
			driverInfo.selectYearLicensed("3 years or more");
			Thread.sleep(2000);
			driverInfo.chooseAccidentsOrClaim();
			Thread.sleep(2000);
			driverInfo.chooseTicketsOrVoilation();

			ScreenshotUtility.takeSnapShot(driver, "Progressive DriverInformationPage");
			Thread.sleep(2000);

			driverInfo.selectContinueButton();
			Thread.sleep(2000);
			driverInfo.selectContinueButton();
			Thread.sleep(2000);
			driverInfo.selectContinueButton();
			Thread.sleep(2000);

			test.log(Status.INFO, "Executing InsuranceDetailPage test");

			// Create an object of InsuranceDetailPage to utilize UI element
			InsuranceDetailPage insuranceDetail = new InsuranceDetailPage(driver);
			if (driver.getTitle().contains("Additional Details")) {
				test.pass("Navigate to InsuranceDetailPage");
			} else {
				test.fail("Error loading InsuranceDetailPage");
			}

			extent.flush();

			insuranceDetail.haveAutoInsurance();
			Thread.sleep(2000);
			insuranceDetail.selectCurrentInsurancePeriod("1 to 3 years");
			Thread.sleep(2000);
			insuranceDetail.selectCurrentBodyInjuryLimit("$50,000/$100,000");
			Thread.sleep(2000);
			insuranceDetail.chooseNonAutoPolicy();
			Thread.sleep(2000);
			insuranceDetail.chooseAutoInsuranceWithProgressive();
			Thread.sleep(2000);
			insuranceDetail.selectDate("08/20/2020");
			Thread.sleep(2000);
			insuranceDetail.primaryEmailAddress("testing123@gmail.com");
			Thread.sleep(2000);
			insuranceDetail.selectNumberOfResident("1");
			Thread.sleep(2000);
			insuranceDetail.selectInjuryClaim("0");

			ScreenshotUtility.takeSnapShot(driver, "Progressive InsuranceDetailPage");
			Thread.sleep(2000);
			insuranceDetail.selectContinueButton();
			Thread.sleep(2000);
			insuranceDetail.chooseEnrollSnapshot();
			Thread.sleep(2000);
			insuranceDetail.selectContinueButton();
			Thread.sleep(2000);
			insuranceDetail.selectNoThanks();

		} catch (NoSuchElementException Nse) {
			Nse.printStackTrace();
			Nse.getCause();
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}

	}

	@AfterTest
	public void AfterTest() throws InterruptedException {

		Thread.sleep(2000);

		driver.close();

		// Setting the properties to pass if scripts get to this point
		PropertiesFile.setProperties();

	}

}
