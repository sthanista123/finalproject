package progressivePagesTest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

//import config.PropertiesConfigurationWithTestNg;

public class PropertiesFile {

	//Create an object of class properties
	static Properties prop = new Properties();
	
	//get system project path
	static String projectPath = System.getProperty("user.dir");
	
	public static void main(String[]args) {
	
		//get current properties
		getProperties();
		
		//set current Properties
		setProperties();
	}

	
	public static void getProperties() {
		try {
			//Create an object to retrive input value from properties
			InputStream input = new FileInputStream(projectPath + "/src/test/java/progressivePagesTest/config.properties");
			
			//load properties File
			prop.load(input);
			
			//Get value from the properties file
			String browser = prop.getProperty("browser");
			
			System.out.println(browser+" is Invoked");
			
			//Set browser for test cases
			StartAQuoteTestNg.browserName = browser;
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
			
		}
		
	}
	
	public static void setProperties() {
		try {
			
			//Create an object of Output stream
			OutputStream output = new FileOutputStream(projectPath + "/src/test/java/progressivePagesTest/config.properties");
			
			//Set values
			prop.setProperty("result", "pass");
			
			//
			prop.store(output, null);
			
			
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
			
		}
		
		
		
	}

}
