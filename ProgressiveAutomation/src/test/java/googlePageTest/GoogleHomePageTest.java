package googlePageTest;

import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;

import googlePage.GoogleHomePage;

public class GoogleHomePageTest {
	
	static WebDriver driver;
	
	@BeforeTest
	public void BeforeTest() {
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		driver = new ChromeDriver();
		
	}
	
	@Test(priority = 0)
	public void performTesting() {
		try {
			driver.get("https://www.google.com/");
			driver.manage().window().maximize();
			
			GoogleHomePage page = new GoogleHomePage(driver);
			page.performSearch("TestNg");
			
		}
		catch(Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
			
		
	}

}
