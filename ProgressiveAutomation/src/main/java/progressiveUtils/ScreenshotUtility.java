package progressiveUtils;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ScreenshotUtility {
	public static void takeSnapShot(WebDriver driver, String screenShotName) {
		try {
			// creating File object to take screenshot
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

			// Store new File

			// FileUtils.copyDirectory(src, new File("./ScreenShots/" + screenShotName +
			// ".Jpeg"));
			FileUtils.copyFile(src, new File("./ScreenShots/" + screenShotName + ".Jpeg"));

			Thread.sleep(5000);

			System.out.println("ScreenShot Taken");

		} catch (Exception e) {
			System.out.println("Exception while taking screenshot" + e.getMessage());
			e.printStackTrace();
		}

	}

}
