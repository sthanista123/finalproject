package progressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InsuranceDetailPage {

	WebDriver driver;

	public InsuranceDetailPage(WebDriver driver) {
		this.driver = driver;

		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@id='FinalDetailsEdit_embedded_questions_list_InsuranceToday_Y']")
	WebElement autoInsurance;

	@FindBy(xpath = "//select[@id='FinalDetailsEdit_embedded_questions_list_RecentAutoInsuranceCompanyPeriod']")
	WebElement currentInsuranceperiod;

	@FindBy(xpath = "//select[@id='FinalDetailsEdit_embedded_questions_list_BodilyInjuryLimits']")
	WebElement currentBodyInjuryLimit;

	@FindBy(xpath = "//input[@id='FinalDetailsEdit_embedded_questions_list_OtherPolicies_N']")
	WebElement nonAutoPolicy;

	@FindBy(xpath = "//input[@id='FinalDetailsEdit_embedded_questions_list_PriorProgressive_N']")
	WebElement autoInsuranceWithProgressive;

	@FindBy(xpath = "//input[@id='FinalDetailsEdit_embedded_questions_list_AdvancedShopperPolicyEffectiveDate']")
	WebElement datePicker;

	@FindBy(xpath = "//input[@id='FinalDetailsEdit_embedded_questions_list_PrimaryEmailAddress']")
	WebElement emailAddress;

	@FindBy(xpath = "//select[@id='FinalDetailsEdit_embedded_questions_list_TotalResidents']")
	WebElement resident;

	@FindBy(xpath = "//select[@id='FinalDetailsEdit_embedded_questions_list_TotalPipClaimsCount']")
	WebElement injuryClaim;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	WebElement continueButton;

	@FindBy(xpath = "//input[@id='SnapshotEnrollment40Edit_embedded_questions_list_SnapshotPolicyEnrollment_N']")
	WebElement enrollSnapshot;

	@FindBy(xpath = "//button[contains(text(),'No thanks, just auto')]")
	WebElement noThanksButton;

	public void haveAutoInsurance() {
		autoInsurance.click();
	}

	public void selectCurrentInsurancePeriod(String period) {

		currentInsuranceperiod.sendKeys(period);

	}

	public void selectCurrentBodyInjuryLimit(String injuryLimit) {
		currentBodyInjuryLimit.sendKeys(injuryLimit);
	}

	public void chooseNonAutoPolicy() {
		nonAutoPolicy.click();
	}

	public void chooseAutoInsuranceWithProgressive() {
		autoInsuranceWithProgressive.click();
	}

	public void selectDate(String startDate) {
		datePicker.sendKeys(startDate);
	}

	public void primaryEmailAddress(String email) {
		emailAddress.sendKeys(email);
	}

	public void selectNumberOfResident(String residentNumber) {
		resident.sendKeys(residentNumber);
	}

	public void selectInjuryClaim(String claim) {
		injuryClaim.sendKeys(claim);
	}

	public void selectContinueButton() {
		continueButton.click();
	}

	public void chooseEnrollSnapshot() {
		enrollSnapshot.click();
	}

	public void selectNoThanks() {
		noThanksButton.click();
	}

}
