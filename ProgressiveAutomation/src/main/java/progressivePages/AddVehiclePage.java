package progressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddVehiclePage {

	// Create an instance of the driver

	WebDriver driver;

	// Create a constructor to create an object of class
	public AddVehiclePage(WebDriver driver) {
		this.driver = driver;

		// Initialize the Web elements from the class
		PageFactory.initElements(driver, this);
	}
	// Locate all the elements that we are using from this page
	// @Annotation

	@FindBy(xpath = "//li[contains(text(),'2020')]")
	WebElement vehicleYear;

	@FindBy(xpath = "//li[contains(text(),' Porsche ')]")
	WebElement vehicleMake;

	@FindBy(xpath = "//li[contains(text(),' Macan ')]")
	WebElement vehicleModel;

	@FindBy(xpath = "//select[@id='VehiclesNew_embedded_questions_list_BodyStyle']")
	WebElement vehicleBodyType;

	@FindBy(xpath = "//select[@id='VehiclesNew_embedded_questions_list_VehicleUse']")
	WebElement primaryUse;

	@FindBy(xpath = "//input[@type='checkbox']")
	WebElement checkbox;

	@FindBy(xpath = "//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']")
	WebElement ownOrLease;

	@FindBy(xpath = "//select[@id='VehiclesNew_embedded_questions_list_LengthOfOwnership']")
	WebElement period;

	@FindBy(xpath = "//input[@id='VehiclesNew_embedded_questions_list_AutomaticEmergencyBraking_Y']")
	WebElement driverAssistance;

	@FindBy(xpath = "//input[@id='VehiclesNew_embedded_questions_list_BlindSpotWarning_Y']")
	WebElement blindSpotWarning;

	@FindBy(xpath = "//button[contains(text(),'Done')]")
	WebElement doneButton;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	WebElement continueButton;

	// Methods that uses all the Web element of this page

	public WebElement selectVehicleYear() {
		vehicleYear.click();
		return vehicleYear;

	}

	public WebElement selectVehicleMake() {

		if (vehicleMake.isDisplayed()) {

			// vehicleMake.sendKeys(make);
			vehicleMake.click();

		}
		return vehicleMake;

	}

	public WebElement selectVehicleModel() {
		if (vehicleModel.isDisplayed()) {
			vehicleModel.click();

		}
		return vehicleModel;

	}

	public void selectVehicleBodyType(String bodyType) {
		vehicleBodyType.sendKeys(bodyType);
	}

	public void selectPrimaryUseOption(String option) {
		primaryUse.sendKeys(option);
	}

	public void selectCheckbox() {
		checkbox.click();
	}

	public void selectOwnOrLease(String selectOption) {
		ownOrLease.sendKeys(selectOption);
	}

	public void vehiclePeriod(String periodTime) {
		period.sendKeys(periodTime);
	}

	public void selectDriverAssistance() {
		driverAssistance.click();
	}

	public void selectBlindSpotWarning() {
		blindSpotWarning.click();
	}

	public void selectDoneButton() {
		doneButton.click();
	}

	public void selectContinueButton() {
		continueButton.click();
	}

}
