package progressivePages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PersonalInformationPage {

	// Create an instance of the driver
	WebDriver driver;

	// Create a constructor to create object of class
	public PersonalInformationPage(WebDriver driver) {
		this.driver = driver;

		// Initialize the Web Elements from the class
		PageFactory.initElements(driver, this);
	}

	// Locate all the elements that we are using from the page
	// Annotation

	@FindBy(xpath = "(//input[@type='text'])[1]")
	WebElement enterFirstName;

	@FindBy(xpath = "(//input[@type='text'])[3]")
	WebElement enterLastName;

	@FindBy(xpath = "//select[@id='NameAndAddressEdit_embedded_questions_list_Suffix']")
	WebElement chooseSuffix;

	@FindBy(xpath = "(//input[@type='tel'])[1]")
	WebElement enterDateOfBirth;

	@FindBy(xpath = "//label[@id='NameAndAddressEdit_embedded_questions_list_MailingAddress_Label']")

	WebElement enterStreetName;

	@FindBy(xpath = "(//input[@type='text'])[5]")
	WebElement enterApt;

	@FindBy(xpath = "//button[contains(text(),'Okay, start my quote.')]")
	WebElement clickStart;

	// Create methods that uses web element from this page
	public void firstName(String fName) {

		enterFirstName.sendKeys(fName);

	}

	public void lastName(String lName) {
		enterLastName.sendKeys(lName);
	}

	public void suffix(String options) {
		chooseSuffix.sendKeys(options);
	}

	public void dateOfBirth(String date) {
		enterDateOfBirth.sendKeys(date);
		enterDateOfBirth.sendKeys(Keys.TAB);

	}

	public void streetName(String street) {

		if (enterStreetName.isDisplayed()) {
			enterStreetName.sendKeys(street);
		}
	}

	public void apartmentNumber(String aNumber) {
		enterApt.sendKeys(aNumber);
	}

	public void selectButton() {
		clickStart.click();
	}
}
