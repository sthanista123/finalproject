package progressivePages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DriverInformationPage {
	// Create a instance of driver
	WebDriver driver;

	// Create a constructor of driver object of class
	public DriverInformationPage(WebDriver driver) {
		this.driver = driver;

		// Initialize the web elements of the class
		PageFactory.initElements(driver, this);
	}

	// Locate all the elements that we are using from this page
	// @Annotation

	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_F']")
	WebElement gender;

	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_MaritalStatus']")
	WebElement maritalStatus;

	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']")
	WebElement educationLevel;

	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_EmploymentStatus']")
	WebElement employment;

	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_Occupation']")
	WebElement occupation;

	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_SocialSecurityNumber']")
	WebElement ssn;

	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']")
	WebElement residency;

	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_HasPriorAddress']")
	WebElement moved;

	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed']")
	WebElement yearsLicensed;

	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N']")
	WebElement accidentsOrClaims;

	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N']")
	WebElement ticketsOrVoilation;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	WebElement continueButton;

	// Create methods that uses all web elements from this page
	public void chooseGender() {
		gender.click();
	}

	public void selectMaritalStatus(String status) {
		maritalStatus.sendKeys(status);
	}

	public void selectEducationLevel(String level) {
		educationLevel.sendKeys(level);
	}

	public void selectEmploymentStatus(String employmentStatus) {
		employment.sendKeys(employmentStatus);
	}

	public void enterOccupation(String enterOcc) {
		occupation.sendKeys(enterOcc);
		occupation.sendKeys(Keys.TAB);
	}

	public void ssNumber(String enterNumber) {
		ssn.sendKeys(enterNumber);

	}

	public void selectPrimaryResidency(String primaryResidency) {
		residency.sendKeys(primaryResidency);
	}

	public void selectMovedSomewhere(String movedSomewhere) {
		moved.sendKeys(movedSomewhere);
	}

	public void selectYearLicensed(String licensed) {
		yearsLicensed.sendKeys(licensed);
	}

	public void chooseAccidentsOrClaim() {
		accidentsOrClaims.click();
	}

	public void chooseTicketsOrVoilation() {
		ticketsOrVoilation.click();
	}

	public void selectContinueButton() {
		continueButton.click();
	}

}
