package progressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	// Create an instance/object of driver
	WebDriver driver;

	// Create a constructor to create object of class
	public HomePage(WebDriver driver) {
		this.driver = driver;

		// Initialize Web elements from the class
		PageFactory.initElements(driver, this);

	}
	// Locate all the elements that we going to use from this page
	// Annotations

	@FindBy(xpath = "(//span[@class='img'])[1]")
	WebElement selectAuto;

	// Create a method that use WebElement from this page
	public void auto() {
		selectAuto.click();

	}

	public boolean pageTitle() {
		// Check whether page title is True/False
		return driver.getTitle().contains("Quote Auto Insurance, Home-Auto Bundles, & More | Progressive");

	}

}
