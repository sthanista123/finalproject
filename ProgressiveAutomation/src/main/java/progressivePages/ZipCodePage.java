package progressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ZipCodePage {

	// Create an instance of driver
	WebDriver driver;

	// Create a constructor to create object of class
	public ZipCodePage(WebDriver driver) {
		this.driver = driver;

		// Initialize Web Elements from the class
		PageFactory.initElements(driver, this);
	}

	// Locate all the elements that we are using from this page
	// Annotation

	@FindBy(xpath = "(//input[@name='ZipCode'])[1]")
	WebElement enterZip;

	@FindBy(xpath = "(//input[@type='submit'])[1]")
	WebElement getAQuote;

	// Create a method that uses Web Element from this page
	public void zipCode(String zip) {
		enterZip.sendKeys(zip);
	}

	public void submitQuote() {
		getAQuote.click();
	}

}
