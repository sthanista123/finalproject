package googlePage;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleHomePage {
	// Create an instance of driver
	WebDriver driver;

	// Create a constructor to create an object of class
	public GoogleHomePage(WebDriver driver) {
		this.driver = driver;

		// Initialize the web elements from the class
		PageFactory.initElements(driver, this);
	}

	// Locate elements present in this page
	// @Annotation

	@FindBy(xpath = "//input[@name='q']")
	WebElement searchField;

	@FindBy(xpath = "(//input[@name='btnK'])[2]")
	WebElement searchBtn;

	public void performSearch(String search) {
		searchField.sendKeys(search);
		searchBtn.sendKeys(Keys.RETURN);

	}

}
